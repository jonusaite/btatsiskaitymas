<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GroupUser2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create('group_user', function (Blueprint $table) {	
	    	$table->increments('id');
	    	$table->integer('group_id');
	    	$table->integer('user_id');
	    	$table->integer('company_id')->nullable();
	    	$table->rememberToken();
	    	$table->timestamps();
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::dropIfExists('group_user');
    }
}
