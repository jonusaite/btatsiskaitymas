## Baltic Talents PHP course final project 

System for Baltic Talents students database. Registration, role confirmation (student or teacher), adding (editing, deleting) courses, groups (adding/deleting students to groups), internships (assigning internships to students), sending e-mails and mass e-mails, contacts (edit contact information with tinymce). Language: LT.

1. Install and configure Laravel. Instructions: [https://laravel.com/docs/5.4](https://laravel.com/docs/5.4)

2. Copy over files from repo.

3. Migrate databases. Instructions: [https://laravel.com/docs/5.4/migrations](https://laravel.com/docs/5.4/migrations)

4. Create new user and change it's "role" in "users" table to "1" for admin.

Copyright (c) 2017 Aistė Jonušaitė

Licensed under [MIT License](https://opensource.org/licenses/mit-license.html)