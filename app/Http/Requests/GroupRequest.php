<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        	'course' => 'required',
        	'starts' => 'required|date_format:Y-m-d',
        	'ends' => 'required|date_format:Y-m-d'
        ];
    }
    
    public function messages()
    {
    	return [
    			'course.required' => 'Pasirinkite kursą',
    			'starts.required' => 'Nurodykite pradžios datą',
    			'ends.required' => 'Nurodykite pabaigos datą',
    			'starts.date_format' => 'Nurodykite datą formatu :format',
    			'ends.date_format' => 'Nurodykite datą formatu :format'
    	];
    }
}
