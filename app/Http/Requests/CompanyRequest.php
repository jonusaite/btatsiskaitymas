<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        		'company' => 'required|max:190',
        		'phone' => 'max:20',
        		'address' => 'max:190',
        		'city' => 'max:190'
        ];
    }
    
    public function messages()
    {
    	return [
    			'company.required' => 'Nurodykite įmonės pavadinimą',
    			'company.max' => 'Įmonės pavadinimas turi būti ne ilgesnis nei :max simbolių',
    			'phone.max' => 'Telefonas turi būti ne ilgesnis nei :max simbolių',
    			'address.max' => 'Adresas turi būti ne ilgesnis nei :max simbolių',
    			'city.max' => 'Miestas turi būti ne ilgesnis nei :max simbolių'
    	];
    }
}
