<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'recipient' => 'required',
        	'message' => 'required',
        	'subject' => 'max:190'
        ];
    }
    public function messages()
    {
    	return [
    			'recipient.required' => 'Pasirinkite gavėją',
    			'message.required' => 'Žinutės laukas privalomas',
    			'subject.max' => 'Tema turi būti ne ilgesnė nei :max simbolių'
    	];
    }
}
