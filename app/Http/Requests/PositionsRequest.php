<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PositionsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        		'course' => 'required',
        		'positions' => 'required|integer|min:0'
        ];
    }
    
    public function messages()
    {
    	return [
    			'course.required' => 'Pasirinkite kursą',
    			'positions.required' => 'Nurodykite vietų skaičių',
    			'positions.integer' => 'Vietų skaičius turi būti skaičius',
    			'positions.min' => 'Vietų skaičius turi būti ne mažesnis už :min'
    	];
    }
}
