<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Mail\AccountConfirmed;
use Illuminate\Support\Facades\Mail;
use App\Course;
use App\Http\Requests\CourseRequest;
use App\Group;
use App\Http\Requests\GroupRequest;
use App\Company;
use App\Http\Requests\StudentCompanyRequest;
use App\Http\Requests\CompanyRequest;
use App\Http\Requests\PositionsRequest;
use App\Http\Requests\LecturerRequest;
use App\Http\Requests\RoleRequest;
use App\Http\Requests\GroupStudentsRequest;
use Illuminate\Support\Facades\Auth;

class AdministratorController extends Controller
{
	public function addRoleView() {
		return view('roles', array(
				'users' => User::all()
		));
	}
	
    public function addRole(RoleRequest $request) {
    	$user = User::find($request->id);
    	$user->role = $request->role;
    	$user->save();
    	if ($request->role == 1) $role = 'Dėstytojas / administratorius';
    		elseif ($request->role == 2) $role = 'Studentas';
    		else return redirect()->back();
    	Mail::to($user->email)->send(new AccountConfirmed($role));
    	return redirect()->back();
    }
    
    public function deleteRole($id) {
    	if (Auth::user()->id != $id) {
	    	$user = User::find($id);
	    	$user->role = NULL;
	    	$user->save();
    	}
    	return redirect()->back();
    }
    
    public function viewCourses() {
    	return view('courses', array(
    			'courses' => Course::all()
    	));
    }
    
    public function addCourse(CourseRequest $request) {
    	$course = new Course();
    	$course->course = $request->course;
    	$course->save();
    	return redirect()->back();
    }
    
    public function deleteCourse($id) {
    	$course = Course::find($id);
    	if (!count($course->group)) {
    		$course->delete();
    	}
    	return redirect()->back();
    }
    
    public function viewCourse($id) {
    	return view('course', array(
    			'course' => Course::find($id)
    			));
    }
    
    public function editCourse($id, CourseRequest $request) {
    	$course = Course::find($id);
    	$course->course = $request->course;
    	$course->save();
    	return redirect()->route('courses');
    }
    
    public function viewGroups() {
    	return view('groups', array(
    			'groups' => Group::all(),
    			'courses' => Course::all()
    	));
    }
    
    public function viewCourseGroups($id) {
    	return view('groups', array(
    			'groups' => Group::where('course_id', $id)->get(),
    			'courses' => Course::all()
    	));
    }
    
    public function addGroup(GroupRequest $request) {
    	$group = new Group();
    	$group->course_id = $request->course;
    	$group->starts = $request->starts;
    	$group->ends = $request->ends;
    	$group->save();
    	return redirect()->back();
    }
    
    public function deleteGroup($id) {
    	$group= Group::find($id);
    	if (!count($group->user)) {
    		$group->delete();
    	}
    	return redirect()->back();
    }
    
    public function viewGroup($id) {
    	return view('group', array(
    			'group' => Group::find($id),
    			'courses' => Course::all(),
    			'lecturers' => User::where('role', 1)->get()
    	));
    }
    
    public function editGroup($id, GroupRequest $request) {
    	$group = Group::find($id);
    	$group->course_id = $request->course;
    	$group->starts = $request->starts;
    	$group->ends = $request->ends;
    	$group->save();
    	return redirect()->route('groups');
    }
    
    public function addLecturer($id, LecturerRequest $request) {
    	$group = Group::find($id);
    	if (!$group->lecturer()->where('user_id', $request->lecturer)->count()) {
    		$group->lecturer()->attach($request->lecturer);
    	}
    	return redirect()->back();
    }
    
    public function deleteLecturer($id, $lecturerId) {
    	$group = Group::find($id);
    	$group->lecturer()->detach($lecturerId);
    	return redirect()->back();
    }
    
    public function groupStudents($id) {
    	return view('groupStudents', array(
    			'group' => Group::find($id)
    	));
    }
    
    public function addGroupStudentsView($id) {
    	$students = User::where('role', 2)->
    		whereDoesntHave('group', function($query) use($id) {
    			$query->where('group_id', $id);
    		})->get();
    	return view('addGroupStudents', array(
    			'group' => Group::find($id),
    			'students' => $students
    	));
    }
    
    public function addGroupStudents($id, GroupStudentsRequest $request) {
    	$group = Group::find($id);
    	foreach ($request->id as $studentId => $on) {
    		if (!$group->user()->where('user_id', $studentId)->count()) {
    			$group->user()->attach($studentId);
    		}
    	}
    	return redirect()->back();
    }
    
    public function deleteGroupStudent($id, $studentId) {
    	$group = Group::find($id);
    	$group->user()->detach($studentId);
    	return redirect()->back();
    }
    
    public function viewCompanies() {
    	return view('companies', array(
    			'companies' => Company::all()
    	));
    }
    
    public function addCompany(CompanyRequest $request) {
    	$company = new Company();
    	$company->company = $request->company;
    	$company->phone = $request->phone;
    	$company->address = $request->address;
    	$company->city = $request->city;
    	$company->save();
    	return redirect()->back();
    }
    
    public function editCompany($id, CompanyRequest $request) {
    	$company = Company::find($id);
    	$company->company = $request->company;
    	$company->phone = $request->phone;
    	$company->address = $request->address;
    	$company->city = $request->city;
    	$company->save();
    	return redirect()->back();
    }
    
    public function deleteCompany($id) {
    	$company = Company::find($id);
    	if (!$company->user->count()) {
    		$company->course()->detach();
    		$company->delete();
    	}
    	return redirect()->back();
    }
    
    public function viewCompany($id) {
    	return view('company', array(
    			'company' => Company::find($id),
    			'courses' => Course::all()
    	));
    }
    
    public function addPositions($id, PositionsRequest $request) {
    	$company = Company::find($id);
    	if (!$company->course()->where('course_id', $request->course)->count()) {
    		$company->course()->attach($request->course, ['positions' => $request->positions]);
    	} else {
    		$company->course()->updateExistingPivot($request->course, ['positions' => $request->positions]);
    	}
    	return redirect()->back();
    }
    
    public function deletePositions($id, $course_id) {
    	$company = Company::find($id);
    	if (!$company->user()->whereHas('group', function($query) use($course_id) {
    		$query->where('course_id', $course_id);
    	})->count()) {
    		$company->course()->detach($course_id);
    	}
    	return redirect()->back();
    }
    
    public function viewStudentCompany($groupId, $studentId) {
    	return view('studentCompany', array(
    			'group_user' => User::find($studentId)->group_user->where('group_id', $groupId)->first()
    	));
    }
    
    public function addStudentCompany($groupId, $studentId, StudentCompanyRequest $request) {
    	$student = User::find($studentId);
    	$company = Company::find($request->company);
    	if ($student->group->where('id', $groupId)->first()->pivot->company_id != $request->company) {
    		if ($student->group->where('id', $groupId)->first()->pivot->company_id) {
    			$oldCompany = Company::find($student->group->where('id', $groupId)->first()->pivot->company_id);
    			$oldCourseId = $student->group->where('id', $groupId)->first()->course->id;
    			$oldPositions = $oldCompany->course->where('id', $oldCourseId)->first()->pivot->positions + 1;
    			$oldCompany->course()->updateExistingPivot($oldCourseId, ['positions' => $oldPositions]);
    		}
	    	$student->group()->updateExistingPivot($groupId, ['company_id' => $request->company]);
	    	$courseId = $student->group->where('id', $groupId)->first()->course->id;
	    	$positions = $company->course->where('id', $courseId)->first()->pivot->positions - 1;
	    	$company->course()->updateExistingPivot($courseId, ['positions' => $positions]);
    	}
    	return redirect()->back();
    }
    
    public function deleteStudentCompany($groupId, $studentId, $companyId) {
    	$student = User::find($studentId);
    	if ($student->group->where('id', $groupId)->first()->pivot->company_id == $companyId) {
	    	$student->group()->updateExistingPivot($groupId, ['company_id' => NULL]);
	    	$company = Company::find($companyId);
	    	$courseId = $student->group->where('id', $groupId)->first()->course->id;
	    	$positions = $company->course->where('id', $courseId)->first()->pivot->positions + 1;
	    	$company->course()->updateExistingPivot($courseId, ['positions' => $positions]);
    	}
    	return redirect()->back();
    }
    
}
