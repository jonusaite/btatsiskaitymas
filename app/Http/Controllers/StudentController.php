<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Http\Requests\StudentCompanyRequest;
use App\Company;

class StudentController extends Controller
{
	public function viewGroups() {
		return view('myGroups', array(
				'groups' => Auth::user()->group
		));
	}
	
	public function viewStudentCompany($groupId) {
		return view('myCompany', array(
				'group_user' => User::find(Auth::user()->id)->group_user->where('group_id', $groupId)->first()
		));
	}
	
	public function addStudentCompany($groupId, StudentCompanyRequest $request) {
		$student = User::find(Auth::user()->id);
		$company = Company::find($request->company);
		if ($student->group->where('id', $groupId)->first()->pivot->company_id != $request->company) {
			if ($student->group->where('id', $groupId)->first()->pivot->company_id) {
				$oldCompany = Company::find($student->group->where('id', $groupId)->first()->pivot->company_id);
				$oldCourseId = $student->group->where('id', $groupId)->first()->course->id;
				$oldPositions = $oldCompany->course->where('id', $oldCourseId)->first()->pivot->positions + 1;
				$oldCompany->course()->updateExistingPivot($oldCourseId, ['positions' => $oldPositions]);
			}
			$student->group()->updateExistingPivot($groupId, ['company_id' => $request->company]);
			$courseId = $student->group->where('id', $groupId)->first()->course->id;
			$positions = $company->course->where('id', $courseId)->first()->pivot->positions - 1;
			$company->course()->updateExistingPivot($courseId, ['positions' => $positions]);
		}
		return redirect()->back();
	}
	
	public function deleteStudentCompany($groupId, $companyId) {
		$student = User::find(Auth::user()->id);
		if ($student->group->where('id', $groupId)->first()->pivot->company_id == $companyId) {
			$student->group()->updateExistingPivot($groupId, ['company_id' => NULL]);
			$company = Company::find($companyId);
			$courseId = $student->group->where('id', $groupId)->first()->course->id;
			$positions = $company->course->where('id', $courseId)->first()->pivot->positions + 1;
			$company->course()->updateExistingPivot($courseId, ['positions' => $positions]);
		}
		return redirect()->back();
	}
	
	public function lecturer($id) {
		if (User::find($id)->group_lecturer()->whereHas('user', function ($query) {
			$query->where('user_id', Auth::user()->id);
		})->count()) {
			return view('lecturer', array(
					'lecturer' => User::find($id)
			));
		} else {
			return redirect()->route('home');
		}
	}
	
}
