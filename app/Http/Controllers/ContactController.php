<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use App\Http\Requests\ContactRequest;

class ContactController extends Controller
{
    public function contactView() {
    	return view('contact', array(
	    		'contact' => Contact::orderBy('created_at', 'desc')->first()
	    ));
    }
    
    public function editContact() {
    	return view('editContact', array(
    			'contact' => Contact::orderBy('created_at', 'desc')->first()
    	));
    }
    
    public function saveContact(ContactRequest $request) {
    	if (Contact::count()) {
	    	$contact = Contact::orderBy('created_at', 'desc')->first();
    	} else {
    		$contact = new Contact();
    	}
	    	$contact->subject = $request->subject;
	    	$contact->content = preg_replace(array('/{/', '/}/'), array('&#123;&zwnj;', '&#125;&zwnj;'), $request->content);
	    	$contact->save();
	    	return redirect()->route('contact');
    	
    }
}
