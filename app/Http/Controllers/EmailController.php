<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\SendEmail;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\User;
use App\Http\Requests\EmailRequest;
use App\Message;
use Illuminate\Support\Facades\Auth;
use App\Group;

class EmailController extends Controller
{
	public function viewSendEmail() {
		return view('emails.email', array(
			'users' => User::all()
		));
	}
	
	public function email(EmailRequest $request) {
		if ($request->recipient) {
			$message = new Message();
			$message->sender_id = Auth::user()->id;
			if ($request->subject) {
				$message->subject = $request->subject;
			} else {
				$message->subject = 'Gauta nauja žinutė';
			}
			$message->message = preg_replace(array('/{/', '/}/'), array('&#123;&zwnj;', '&#125;&zwnj;'), $request->message);
			$message->save();
			$recipient = User::find($request->recipient)->email;
			$message->user()->attach($request->recipient);
			Mail::to($recipient)->send(new SendEmail($message));
		}
		return redirect()->back();
	}
	
	public function viewSendMassEmail() {
		if (Auth::user()->role == 1) {
			return view('emails.massEmail', array(
				'groups' => Group::has('user')->get()
			));
		} elseif (Auth::user()->role == 2) {
			return view('emails.massEmail', array(
					'groups' => Auth::user()->group
			));
		} else {
			echo Auth::user()->role;
		}
		
	}
	
	public function sendMassEmail(EmailRequest $request) {
		if ($request->recipient) {
			$group = Group::find($request->recipient);
			if ($group->user->count()) {
				$message = new Message();
				$message->sender_id = Auth::user()->id;
				if ($request->subject) {
					$message->subject = $request->subject;
				} else {
					$message->subject = 'Gauta nauja žinutė';
				}
				$message->message = preg_replace(array('/{/', '/}/'), array('&#123;&zwnj;', '&#125;&zwnj;'), $request->message);
				$message->save();
				foreach ($group->user as $user) {
					$recipients[] = $user->email;
					$message->user()->attach($user->id);
				}
				Mail::to('undinele1990@gmail.com')
				->bcc($recipients)
				->send(new SendEmail($message));
			}
		}
		return redirect()->back();
	}
	
	public function emailReceived() {
		return view('emails.emailReceived', array(
				'messages' => Auth::user()->message()->orderBy('created_at', 'desc')->get()
		));
	}
	
	public function emailSent() {
		return view('emails.emailSent', array(
				'messages' => Auth::user()->sendersMessage()->orderBy('created_at', 'desc')->get()
		));
	}
	
}
