<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AccountConfirmed extends Mailable
{
    use Queueable, SerializesModels;

    public $role;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($role)
    {
        $this->role = $role;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
    	return $this->subject('Paskyra patvirtinta')->
    				markdown('emails.confirmation')->
    				with([
    						'url' => route('login'),
    						'role' => $this->role
    					]);
    }
}
