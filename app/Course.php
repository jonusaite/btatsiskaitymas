<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    public function group() {
    	return $this->hasMany('App\Group');
    }
    
    public function company() {
    	return $this->belongsToMany('App\Company')->withPivot('positions')->withTimestamps();
    }
}
