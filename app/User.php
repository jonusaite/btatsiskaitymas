<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'email', 'phone', 'address', 'city', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function group() {
    	return $this->belongsToMany('App\Group')->withPivot('company_id')->withTimestamps();
    }
    
    public function company() {
    	return $this->belongsToMany('App\Company', 'group_user')->withPivot('group_id')->withTimestamps();
    }
    
    public function group_user() {
    	return $this->hasMany('App\Group_User');
    }
    
    public function group_lecturer() {
    	return $this->belongsToMany('App\Group', 'group_lecturer')->withTimestamps();
    }
    
    public function message() {
    	return $this->belongsToMany('App\Message')->withTimestamps();
    }
    
    public function sendersMessage() {
    	return $this->hasMany('App\Message', 'sender_id');
    }
    
}
