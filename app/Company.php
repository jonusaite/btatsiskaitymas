<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
	public function course() {
		return $this->belongsToMany('App\Course')->withPivot('positions')->withTimestamps();
	}
	
	public function group_user() {
		return $this->hasMany('App\Group_User');
	}
	
	public function user() {
		return $this->belongsToMany('App\User', 'group_user')->withPivot('group_id')->withTimestamps();
	}
	
	public function group() {
		return $this->belongsToMany('App\Group', 'group_user')->withPivot('user_id')->withTimestamps();
	}

}
