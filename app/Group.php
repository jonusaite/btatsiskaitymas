<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
	public function course() {
		return $this->belongsTo('App\Course');
	}
	
	public function user() {
		return $this->belongsToMany('App\User')->withPivot('company_id')->withTimestamps();
	}
	
	public function company() {
		return $this->belongsToMany('App\Company', 'group_user')->withPivot('user_id')->withTimestamps();
	}
	
	public function group_user() {
		return $this->hasMany('App\Group_User');
	}
	
	public function lecturer() {
		return $this->belongsToMany('App\User', 'group_lecturer')->withTimestamps();
	}
}
