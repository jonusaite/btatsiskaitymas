@extends('layouts.app')
@section('content')

<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading">
	    	<h3 class="panel-title">Grupės <strong> {{ $group->id }} {{ $group->course->course }} {{ $group->starts }} – {{ $group->ends }}</strong> dėstytojai
	    	</h3>
		</div>
		<div class="panel-body table-responsive">
			<table class="table table-hover">
		    	<thead>
		    		<tr>
			    		<th>Vardas</th>
			    		<th>Pavardė</th>
			    		<th>El. paštas</th>
			    		<th>Telefonas</th>
			    		<th>Adresas</th>
			    		<th>Miestas</th>
			    		<th>Veiksmai</th>
			    	</tr>
			    </thead>
			    <tbody>
			    	@foreach ($group->lecturer as $lecturer)
			    	<tr>
			    		<td>{{ $lecturer->name }}</td>
			    		<td>{{ $lecturer->surname }}</td>
			    		<td>{{ $lecturer->email }}</td>
			    		<td>{{ $lecturer->phone }}</td>
			    		<td>{{ $lecturer->address }}</td>
			    		<td>{{ $lecturer->city }}</td>
			    		<td>
			    		<a class="btn btn-default" href="{{ route('deleteLecturer', [$group->id, $lecturer->id]) }}"><i class="fa fa-times" aria-hidden="true"></i> Ištrinti</a>
			    		</td>
			    	</tr>
			    	@endforeach
			    </tbody>
			</table>

		</div>
	</div>
</div>


<div class="col-md-6">
	<div class="panel panel-default">
		<div class="panel-heading">
	    	<h3 class="panel-title">Redaguoti grupę</h3>
		</div>
		<div class="panel-body">
		
			<form class="form-horizontal" role="form" action="{{ route('editGroup', $group->id) }}" method="post">
				{{ csrf_field() }}
				
				<div class="form-group{{ $errors->has('course') ? ' has-error' : '' }}">
					<label class="col-sm-4 control-label">Kursas</label>
					<div class="col-sm-6">
						<select name="course" class="form-control">
							<option value="">- Pasirinkite kursą</option>
							@foreach ($courses as $course)
								<option value="{{ $course->id }}"{{ $group->course_id == $course->id ? ' selected' : '' }}>{{ $course->course }}</option>
							@endforeach
						</select>
						@if ($errors->has('course'))
                        	<span class="help-block">
                            	<strong>{{ $errors->first('course') }}</strong>
							</span>
                        @endif
					</div>
				</div>
				
				<div class="form-group{{ $errors->has('starts') ? ' has-error' : '' }}">
					<label class="col-sm-4 control-label">Pradžios data</label>
					<div class="col-sm-6">
						<input name="starts" type="text" value="{{ $group->starts }}">
						@if ($errors->has('starts'))
                        	<span class="help-block">
                            	<strong>{{ $errors->first('starts') }}</strong>
							</span>
                        @endif
					</div>
				</div>
				
				<div class="form-group{{ $errors->has('ends') ? ' has-error' : '' }}">
					<label class="col-sm-4 control-label">Pabaigos data</label>
					<div class="col-sm-6">
						<input name="ends" type="text" value="{{ $group->ends }}">
						@if ($errors->has('ends'))
                        	<span class="help-block">
                            	<strong>{{ $errors->first('ends') }}</strong>
							</span>
                        @endif
					</div>
				</div>
				
				<div class="input-group">
					<div class="col-sm-6 col-md-offset-4">
						<input type="submit" class="btn btn-primary" value="Išsaugoti">
					</div>
				</div>
				
			</form>
		
		</div>
	</div>
</div>

<div class="col-md-6">
	<div class="panel panel-default">
		<div class="panel-heading">
	    	<h3 class="panel-title">Pridėti dėstytoją</h3>
		</div>
		<div class="panel-body">
		
			<form class="form-horizontal" role="form" action="{{ route('addLecturer', $group->id) }}" method="post">
				{{ csrf_field() }}
				
				<div class="form-group{{ $errors->has('lecturer') ? ' has-error' : '' }}">
					<label class="col-sm-4 control-label">Dėstytojas</label>
					<div class="col-sm-6">
						<select name="lecturer" class="form-control">
							<option value="">- Pasirinkite dėstytoją</option>
							@foreach ($lecturers as $lecturer)
								<option value="{{ $lecturer->id }}">{{ $lecturer->name }} {{ $lecturer->surname }}</option>
							@endforeach
						</select>
						@if ($errors->has('lecturer'))
                        	<span class="help-block">
                            	<strong>{{ $errors->first('lecturer') }}</strong>
							</span>
                        @endif
					</div>
				</div>
				
				<div class="input-group">
					<div class="col-sm-6 col-md-offset-4">
						<input type="submit" class="btn btn-primary" value="Pridėti">
					</div>
				</div>
				
			</form>
		
		</div>
	</div>
</div>

@endsection