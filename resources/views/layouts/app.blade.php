<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Baltic Talents</title>

    <!-- Styles -->
    <link href="{{ asset(mix('/css/app.css')) }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ route('home') }}">
                        Baltic Talents
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        @if (!Auth::guest())
                        
							@if ((Auth::user()->role == 1) || (Auth::user()->role == 2))
                        	<li class="dropdown">
	                        	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
	                                    Laiškai <span class="caret"></span>
	                            </a>
	                        	
	                        	<ul class="dropdown-menu" role="menu">
	                        		@if (Auth::user()->role == 1)
	                            		<li><a href="{{ route('email') }}">Siųsti laišką</a></li>
	                            	@endif
	                            	<li><a href="{{ route('massEmail') }}">Siųsti masinį laišką</a></li>
	                            	<li><a href="{{ route('emailReceived') }}">Gauti laiškai</a></li>
	                            	<li><a href="{{ route('emailSent') }}">Išsiųsti laiškai</a></li>
	                        	</ul>
                        	
                        	</li>
                        	@endif
                        	
                        	@if (Auth::user()->role == 1)
                        	<li><a href="{{ route('roles') }}">Vartotojai</a></li>
                        	<li><a href="{{ route('courses') }}">Kursai</a></li>
                        	<li><a href="{{ route('groups') }}">Grupės</a></li>
                        	<li><a href="{{ route('companies') }}">Įmonės</a></li>
                        	@elseif (Auth::user()->role == 2)
                        	<li><a href="{{ route('myGroups') }}">Mano grupės</a></li>
                        	@endif
                        	
                        @endif
                        <li><a href="{{ route('contact') }}">Kontaktai</a></li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Prisijungti</a></li>
                            <li><a href="{{ route('register') }}">Registruotis</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Atsijungti
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
		<div class="container">
        @yield('content')
        </div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset(mix('/tinymce/tinymce.min.js')) }}"></script>
    <script src="{{ asset(mix('/js/app.js')) }}"></script>
  	
    
    
</body>
</html>
