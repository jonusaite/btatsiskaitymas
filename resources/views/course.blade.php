@extends('layouts.app')
@section('content')
<div class="col-md-8 col-md-offset-2">
	<div class="panel panel-default">
		<div class="panel-heading">
	    	<h3 class="panel-title">Redaguoti kursą</h3>
		</div>
		<div class="panel-body">
		
			<form class="form-horizontal" role="form" action="{{ route('editCourse', $course->id) }}" method="post">
				{{ csrf_field() }}
				
				<div class="form-group{{ $errors->has('course') ? ' has-error' : '' }}">
					<label class="col-sm-4 control-label">Kursas</label>
					<div class="col-sm-6">
						<input class="form-control" name="course" type="text" value="{{ $course->course }}">
						@if ($errors->has('course'))
                        	<span class="help-block">
                            	<strong>{{ $errors->first('course') }}</strong>
							</span>
                        @endif
					</div>
				</div>
				
				<div class="input-group">
					<div class="col-sm-6 col-md-offset-4">
						<input type="submit" class="btn btn-primary" value="Išsaugoti">
					</div>
				</div>
				
			</form>
		
		</div>
	</div>
</div>

@endsection