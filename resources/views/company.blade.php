@extends('layouts.app')
@section('content')
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading">
	    	<h3 class="panel-title">Vietų skaičius <strong>{{ $company->company }}</strong> įmonėje</h3>
		</div>
		<div class="panel-body table-responsive">
			<table class="table table-hover">
		    	<thead>
		    		<tr>
		    			<th>Kursas</th>
		    			<th>Laisvų vietų skaičius</th>
			    		<th>Veiksmai</th>
			    	</tr>
			    </thead>
			    <tbody>
			    	@foreach ($company->course as $course)
			    	<tr>
						<td>{{ $course->course }}</td>
						<td>{{ $course->pivot->positions }}</td>
			    		<td>
			    		@if (!$company->group()->where('course_id', $course->id)->count())
			    			<a class="btn btn-default" href="{{ route('deletePositions', [$company->id, $course->id]) }}"><i class="fa fa-times" aria-hidden="true"></i> Ištrinti</a>
			    		@endif
			    		</td>
			    	</tr>
			    	@endforeach
			    </tbody>
			</table>

		</div>
	</div>
</div>

<div class="col-md-6">
	<div class="panel panel-default">
		<div class="panel-heading">
	    	<h3 class="panel-title">Redaguoti įmonę</h3>
		</div>
		<div class="panel-body">
		
			<form class="form-horizontal" role="form" action="{{ route('editCompany', $company->id) }}" method="post">
				{{ csrf_field() }}
				
				<div class="form-group{{ $errors->has('company') ? ' has-error' : '' }}">
					<label class="col-sm-4 control-label">Įmonė</label>
					<div class="col-sm-6">
						<input class="form-control" name="company" type="text" value="{{ $company->company }}">
						@if ($errors->has('company'))
                        	<span class="help-block">
                            	<strong>{{ $errors->first('company') }}</strong>
							</span>
                        @endif
					</div>
				</div>
				
				<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                	<label for="phone" class="col-sm-4 control-label">Telefonas</label>
					<div class="col-sm-6">
                        <input id="phone" type="text" class="form-control" name="phone" value="{{ $company->phone }}">
						@if ($errors->has('phone'))
                        	<span class="help-block">
                            	<strong>{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
					</div>
				</div>
                        
				<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
					<label for="address" class="col-sm-4 control-label">Adresas</label>
					<div class="col-sm-6">
						<input id="address" type="text" class="form-control" name="address" value="{{ $company->address }}">
						@if ($errors->has('address'))
							<span class="help-block">
								<strong>{{ $errors->first('address') }}</strong>
							</span>
						@endif
					</div>
				</div>
				
				<div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
					<label for="city" class="col-sm-4 control-label">Miestas</label>
					<div class="col-sm-6">
						<input id="city" type="text" class="form-control" name="city" value="{{ $company->city }}">
						@if ($errors->has('city'))
							<span class="help-block">
								<strong>{{ $errors->first('city') }}</strong>
							</span>
						@endif
					</div>
				</div>
				
				<div class="input-group">
					<div class="col-sm-6 col-md-offset-4">
						<input type="submit" class="btn btn-primary" value="Išsaugoti">
					</div>
				</div>
				
			</form>
		
		</div>
	</div>
</div>

<div class="col-md-6">
	<div class="panel panel-default">
		<div class="panel-heading">
	    	<h3 class="panel-title">Pridėti / redaguoti vietas</h3>
		</div>
		<div class="panel-body">
		
			<form class="form-horizontal" role="form" action="{{ route('addPositions', $company->id) }}" method="post">
				{{ csrf_field() }}
				
				<div class="form-group{{ $errors->has('course') ? ' has-error' : '' }}">
					<label class="col-sm-4 control-label">Kursas</label>
					<div class="col-sm-6">
						<select name="course" class="form-control">
							<option value="">- Pasirinkite kursą</option>
							@foreach ($courses as $course)
								<option value="{{ $course->id }}"{{ old('course') == $course->id ? ' selected' : '' }}>{{ $course->course }}</option>
							@endforeach
						</select>
						@if ($errors->has('course'))
                        	<span class="help-block">
                            	<strong>{{ $errors->first('course') }}</strong>
							</span>
                        @endif
					</div>
				</div>
				
				<div class="form-group{{ $errors->has('positions') ? ' has-error' : '' }}">
                	<label for="phone" class="col-sm-4 control-label">Vietų skaičius</label>
					<div class="col-sm-6">
                        <input id="positions" type="text" class="form-control" name="positions" value="{{ old('positions') }}">
						@if ($errors->has('positions'))
                        	<span class="help-block">
                            	<strong>{{ $errors->first('positions') }}</strong>
                            </span>
                        @endif
					</div>
				</div>
				
				<div class="input-group">
					<div class="col-sm-6 col-md-offset-4">
						<input type="submit" class="btn btn-primary" value="Pridėti">
					</div>
				</div>
				
			</form>
		
		</div>
	</div>
</div>

@endsection