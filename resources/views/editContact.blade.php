@extends('layouts.app')
@section('content')

<div class="col-md-8 col-md-offset-2">
	<div class="panel panel-default">
		<div class="panel-heading">
	    	<h3 class="panel-title">Redaguoti kontaktinę informaciją</h3>
		</div>
		<div class="panel-body">

			<form  class="form-horizontal" role="form" action="{{ route('saveContact') }}" method="post">
				{{ csrf_field() }}
				
				<div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
					<label class="col-sm-4 control-label">Pavadinimas</label>
					<div class="col-sm-6">
						<input class="form-control" name="subject" type="text" value="{{ old('subject') ? old('subject') : (isset($contact) ? $contact->subject : '') }}">
						@if ($errors->has('subject'))
                        	<span class="help-block">
                            	<strong>{{ $errors->first('subject') }}</strong>
							</span>
                        @endif
					</div>
				</div>
				
				<div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
					<div class="col-sm-12">
						<textarea id="editor" class="form-control" rows="5" name="content">{{ htmlentities(old('content') ? old('content') : (isset($contact) ? $contact->content : '')) }}</textarea>
						@if ($errors->has('content'))
                        	<span class="help-block">
                            	<strong>{{ $errors->first('content') }}</strong>
							</span>
                        @endif
					</div>
				</div>
				
				<div class="input-group">
					<div class="col-sm-6 col-md-offset-4">
						<input type="submit" class="btn btn-primary" value="Išsaugoti">
					</div>
				</div>
				
			</form>
		
		</div>
	</div>
</div>

@endsection