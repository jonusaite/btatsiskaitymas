@extends('layouts.app')
@section('content')

<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading">
	    	<h3 class="panel-title">Pridėti studentų į grupę <strong> <a href="{{ route('groupStudents', $group->id) }}">{{ $group->id }} {{ $group->course->course }} {{ $group->starts }} – {{ $group->ends }}</a></strong>
	    	</h3>
		</div>
		<div class="panel-body table-responsive">
			<form class="form-horizontal" role="form" action="{{ route('addGroupStudents', $group->id) }}" method="post">
				{{ csrf_field() }}
				
				<table class="table table-hover">
			    	<thead>
			    		<tr>
			    			<th></th>
				    		<th>Vardas</th>
				    		<th>Pavardė</th>
				    		<th>El. paštas</th>
				    		<th>Telefonas</th>
				    		<th>Adresas</th>
				    		<th>Miestas</th>
				    	</tr>
				    </thead>
				    <tbody>
				    	@foreach ($students as $student)
				    		<tr>
						    	<td><input type="checkbox" name="id[{{ $student->id }}]"></td>
						    	<td>{{ $student->name }}</td>
						    	<td>{{ $student->surname }}</td>
						    	<td>{{ $student->email }}</td>
						    	<td>{{ $student->phone }}</td>
						    	<td>{{ $student->address }}</td>
						    	<td>{{ $student->city }}</td>
						    </tr>
				    	@endforeach
				    </tbody>
				</table>
				
				@if ($errors->has('id'))
				<div class="form-group has-error">
                	<span class="help-block">
                    	<strong>{{ $errors->first('id') }}</strong>
					</span>
				</div>
                @endif
                
				<div class="input-group">
					<div class="col-sm-6 col-md-offset-4">
						<input type="submit" class="btn btn-primary" value="Pridėti">
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection