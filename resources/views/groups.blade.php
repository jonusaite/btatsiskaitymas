@extends('layouts.app')
@section('content')
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading">
	    	<h3 class="panel-title">Grupių sąrašas</h3>
		</div>
		<div class="panel-body table-responsive">
			<table class="table table-hover">
		    	<thead>
		    		<tr>
			    		<th>Grupė</th>
			    		<th>Kursas</th>
			    		<th>Studentų skaičius</th>
			    		<th>Pradžios data</th>
			    		<th>Pabaigos data</th>
			    		<th>Veiksmai</th>
			    	</tr>
			    </thead>
			    <tbody>
			    	@foreach ($groups as $group)
			    	<tr>
			    		<td>{{ $group->id }}</td>
			    		<td><a class="btn btn-default" href="{{ route('viewCourseGroups', $group->course->id) }}">{{ $group->course->course }}</a></td>
			    		<td>{{ count($group->user) }}</td>
			    		<td>{{ $group->starts }}</td>
			    		<td>{{ $group->ends }}</td>
			    		<td>
			    		<a class="btn btn-default" href="{{ route('groupStudents', $group->id) }}"><i class="fa fa-eye" aria-hidden="true"></i> Studentai</a>    		
			    		<a class="btn btn-default" href="{{ route('viewGroup', $group->id) }}"><i class="fa fa-pencil" aria-hidden="true"></i> Redaguoti</a>
			    		@if (!count($group->user))
			    		<a class="btn btn-default" href="{{ route('deleteGroup', $group->id) }}"><i class="fa fa-times"  aria-hidden="true"></i> Ištrinti</a>
			    		@endif
			    		</td>
			    	</tr>
			    	@endforeach
			    </tbody>
			</table>

		</div>
	</div>
</div>
<div class="col-md-8 col-md-offset-2">
	<div class="panel panel-default">
		<div class="panel-heading">
	    	<h3 class="panel-title">Pridėti grupę</h3>
		</div>
		<div class="panel-body">
		
			<form class="form-horizontal" role="form" action="{{ route('addGroup') }}" method="post">
				{{ csrf_field() }}
				
				<div class="form-group{{ $errors->has('course') ? ' has-error' : '' }}">
					<label class="col-sm-4 control-label">Kursas</label>
					<div class="col-sm-6">
						<select name="course" class="form-control">
							<option value="">- Pasirinkite kursą</option>
							@foreach ($courses as $course)
								<option value="{{ $course->id }}"{{ old('course') == $course->id ? ' selected' : '' }}>{{ $course->course }}</option>
							@endforeach
						</select>
						@if ($errors->has('course'))
                        	<span class="help-block">
                            	<strong>{{ $errors->first('course') }}</strong>
							</span>
                        @endif
					</div>
				</div>
				
				<div class="form-group{{ $errors->has('starts') ? ' has-error' : '' }}">
					<label class="col-sm-4 control-label">Pradžios data</label>
					<div class="col-sm-6">
						<input name="starts" type="text" value="{{ old('starts') }}">
						@if ($errors->has('starts'))
                        	<span class="help-block">
                            	<strong>{{ $errors->first('starts') }}</strong>
							</span>
                        @endif
					</div>
				</div>
				
				<div class="form-group{{ $errors->has('ends') ? ' has-error' : '' }}">
					<label class="col-sm-4 control-label">Pabaigos data</label>
					<div class="col-sm-6">
						<input name="ends" type="text" value="{{ old('ends') }}">
						@if ($errors->has('ends'))
                        	<span class="help-block">
                            	<strong>{{ $errors->first('ends') }}</strong>
							</span>
                        @endif
					</div>
				</div>
				
				<div class="input-group">
					<div class="col-sm-6 col-md-offset-4">
						<input type="submit" class="btn btn-primary" value="Pridėti">
					</div>
				</div>
				
			</form>
		
		</div>
	</div>
</div>

@endsection