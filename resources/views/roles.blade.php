@extends('layouts.app')
@section('content')
<div class="col-md-6 col-md-offset-3">
	<div class="panel panel-default">
		<div class="panel-heading">
	    	<h3 class="panel-title">Priskirti rolę</h3>
		</div>
		<div class="panel-body">
		
			<form class="form-horizontal" role="form" action="{{ route('addRole') }}" method="post">
				{{ csrf_field() }}
				
				<div class="form-group{{ $errors->has('id') ? ' has-error' : '' }}">
					<label class="col-sm-4 control-label">Vartotojas</label>
					<div class="col-sm-6">
						<select name="id" class="form-control">
							<option value="">- Pasirinkite vartotoją</option>
							@foreach ($users as $user)
								@if (!$user->role)
								<option value="{{ $user->id }}"{{ old('id') == $user->id ? ' selected' : '' }}>{{ $user-> name }} {{ $user-> surname }}</option>
								@endif
							@endforeach
						</select>
						@if ($errors->has('id'))
                        	<span class="help-block">
                            	<strong>{{ $errors->first('id') }}</strong>
							</span>
                        @endif
					</div>
				</div>
				
				<div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
					<label class="col-sm-4 control-label">Rolė</label>
					<div class="col-sm-6">
						<select name="role" class="form-control">
							<option value="">- Pasirinkite rolę</option>
							<option value="1"{{ old('role') == 1 ? ' selected' : '' }}>Dėstytojas / administratorius</option>
							<option value="2"{{ old('role') == 2 ? ' selected' : '' }}>Studentas</option>
						</select>
						@if ($errors->has('role'))
                        	<span class="help-block">
                            	<strong>{{ $errors->first('role') }}</strong>
							</span>
                        @endif
					</div>
				</div>
				
				<div class="input-group">
					<div class="col-sm-6 col-md-offset-4">
						<input type="submit" class="btn btn-primary" value="Priskirti">
					</div>
				</div>
				
			</form>
		
		</div>
	</div>
</div>

<div class="col-md-6">
	<div class="panel panel-default">
		<div class="panel-heading">
	    	<h3 class="panel-title">Dėstytojai / administratoriai</h3>
		</div>
		<div class="panel-body table-responsive">
			<table class="table table-hover">
		    	<thead>
		    		<tr>
		    			<th>Vardas</th>
		    			<th>Pavardė</th>
			    		<th>Veiksmai</th>
			    	</tr>
			    </thead>
			    <tbody>
			    	@foreach ($users as $user)
						@if ($user->role == 1)
				    	<tr>
				    		<td>{{ $user->name }}</td>
				    		<td>{{ $user->surname }}</td>
				    		<td>
				    		@if (Auth::user()->id != $user->id)
				    		<a class="btn btn-default" href="{{ route('deleteRole', $user->id) }}"><i class="fa fa-times"  aria-hidden="true"></i> Ištrinti</a>
				    		@endif
				    		</td>
				    	</tr>
				    	@endif
			    	@endforeach
			    </tbody>
			</table>

		</div>
	</div>
</div>

<div class="col-md-6">
	<div class="panel panel-default">
		<div class="panel-heading">
	    	<h3 class="panel-title">Studentai</h3>
		</div>
		<div class="panel-body table-responsive">
			<table class="table table-hover">
		    	<thead>
		    		<tr>
		    			<th>Vardas</th>
		    			<th>Pavardė</th>
			    		<th>Veiksmai</th>
			    	</tr>
			    </thead>
			    <tbody>
			    	@foreach ($users as $user)
						@if ($user->role == 2)
				    	<tr>
				    		<td>{{ $user->name }}</td>
				    		<td>{{ $user->surname }}</td>
				    		<td>
				    		<a class="btn btn-default" href="{{ route('deleteRole', $user->id) }}"><i class="fa fa-times" aria-hidden="true"></i> Ištrinti</a>
				    		</td>
				    	</tr>
				    	@endif
			    	@endforeach
			    </tbody>
			</table>

		</div>
	</div>
</div>

@endsection