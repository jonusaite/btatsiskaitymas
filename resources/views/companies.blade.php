@extends('layouts.app')
@section('content')
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading">
	    	<h3 class="panel-title">Įmonių sąrašas</h3>
		</div>
		<div class="panel-body table-responsive">
			<table class="table table-hover">
		    	<thead>
		    		<tr>
		    			<th>Įmonė</th>
		    			<th>Telefonas</th>
		    			<th>Adresas</th>
		    			<th>Miestas</th>
			    		<th>Veiksmai</th>
			    	</tr>
			    </thead>
			    <tbody>
			    	@foreach ($companies as $company)
			    	<tr>
			    		<td>{{ $company->company }}</td>
			    		<td>{{ $company->phone }}</td>
			    		<td>{{ $company->address }}</td>
			    		<td>{{ $company->city }}</td>
			    		<td>
			    		<a class="btn btn-default" href="{{ route('company', $company->id) }}"><i class="fa fa-pencil" aria-hidden="true"></i> Redaguoti</a>
			    		@if (!count($company->user))
			    		<a class="btn btn-default" href="{{ route('deleteCompany', $company->id) }}"><i class="fa fa-times"  aria-hidden="true"></i> Ištrinti</a>
			    		@endif
			    		</td>
			    	</tr>
			    	@endforeach
			    </tbody>
			</table>

		</div>
	</div>
</div>
<div class="col-md-8 col-md-offset-2">
	<div class="panel panel-default">
		<div class="panel-heading">
	    	<h3 class="panel-title">Pridėti įmonę</h3>
		</div>
		<div class="panel-body">
		
			<form class="form-horizontal" role="form" action="{{ route('addCompany') }}" method="post">
				{{ csrf_field() }}
				
				<div class="form-group{{ $errors->has('company') ? ' has-error' : '' }}">
					<label class="col-sm-4 control-label">Įmonė</label>
					<div class="col-sm-6">
						<input class="form-control" name="company" type="text" value="{{ old('company') }}">
						@if ($errors->has('company'))
                        	<span class="help-block">
                            	<strong>{{ $errors->first('company') }}</strong>
							</span>
                        @endif
					</div>
				</div>
				
				<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                	<label for="phone" class="col-sm-4 control-label">Telefonas</label>
					<div class="col-sm-6">
                        <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}">
						@if ($errors->has('phone'))
                        	<span class="help-block">
                            	<strong>{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
					</div>
				</div>
                        
				<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
					<label for="address" class="col-sm-4 control-label">Adresas</label>
					<div class="col-sm-6">
						<input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}">
						@if ($errors->has('address'))
							<span class="help-block">
								<strong>{{ $errors->first('address') }}</strong>
							</span>
						@endif
					</div>
				</div>
				
				<div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
					<label for="city" class="col-sm-4 control-label">Miestas</label>
					<div class="col-sm-6">
						<input id="city" type="text" class="form-control" name="city" value="{{ old('city') }}">
						@if ($errors->has('city'))
							<span class="help-block">
								<strong>{{ $errors->first('city') }}</strong>
							</span>
						@endif
					</div>
				</div>
				
				<div class="input-group">
					<div class="col-sm-6 col-md-offset-4">
						<input type="submit" class="btn btn-primary" value="Pridėti">
					</div>
				</div>
				
			</form>
		
		</div>
	</div>
</div>

@endsection