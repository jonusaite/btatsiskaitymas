@extends('layouts.app')
@section('content')
<div class="col-md-6">
	<div class="panel panel-default">
		<div class="panel-heading">
	    	<h3 class="panel-title">Studento <strong>{{ $group_user->user->name }} {{ $group_user->user->surname }}</strong> praktikos vieta <strong><a href="{{ route('groupStudents', $group_user->group_id) }}">{{ $group_user->group->id }} {{ $group_user->group->course->course }} {{ $group_user->group->starts }} – {{ $group_user->group->ends }}</a></strong> grupėje</h3>
		</div>
		<div class="panel-body table-responsive">
			@if ($group_user->company_id)
				Įmonė: {{ $group_user->company->company }} <br>
				Adresas: {{ $group_user->company->address }} <br>
				Telefonas: {{ $group_user->company->phone }} <br>
				Miestas: {{ $group_user->company->city }} <br>
				 <a class="btn btn-default" href="{{ route('deleteStudentCompany', [$group_user->group_id, $group_user->user_id, $group_user->company_id]) }}"><i class="fa fa-times"  aria-hidden="true"></i> Ištrinti</a>
			@endif
		</div>
	</div>
</div>

<div class="col-md-6">
	<div class="panel panel-default">
		<div class="panel-heading">
	    	<h3 class="panel-title">Pridėti praktikos vietą</h3>
		</div>
		<div class="panel-body">
		
			<form class="form-horizontal" role="form" action="{{ route('addStudentCompany', [$group_user->group_id, $group_user->user_id]) }}" method="post">
				{{ csrf_field() }}
				
				<div class="form-group{{ $errors->has('company') ? ' has-error' : '' }}">
					<label class="col-sm-4 control-label">Įmonė</label>
					<div class="col-sm-6">
						<select name="company" class="form-control">
							<option value="">- Pasirinkite įmonę</option>
							@foreach ($group_user->group->course->company as $company)
								@if  ($company->pivot->positions > 0)
								<option value="{{ $company->id }}"{{ old('company') == $company->id ? ' selected' : '' }}>{{ $company->company }}</option>
								@endif
							@endforeach
						</select>
						@if ($errors->has('company'))
                        	<span class="help-block">
                            	<strong>{{ $errors->first('company') }}</strong>
							</span>
                        @endif
					</div>
				</div>
				
				<div class="input-group">
					<div class="col-sm-6 col-md-offset-4">
						<input type="submit" class="btn btn-primary" value="Pridėti">
					</div>
				</div>
				
			</form>
		</div>
	</div>
</div>

@endsection