@extends('layouts.app')
@section('content')
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading">
	    	<h3 class="panel-title">Kursų sąrašas</h3>
		</div>
		<div class="panel-body table-responsive">
			<table class="table table-hover">
		    	<thead>
		    		<tr>
			    		<th>Kursas</th>
			    		<th>Grupių skaičius</th>
			    		<th>Veiksmai</th>
			    	</tr>
			    </thead>
			    <tbody>
			    	@foreach ($courses as $course)
			    	<tr>
			    		<td>{{ $course->course }}</td>
			    		<td>
			    		@if (count($course->group))
						<a class="btn btn-default" href="{{ route('viewCourseGroups', $course->id) }}">{{ count($course->group) }}</a>
						@endif
						</td>
			    		<td>			    		
			    		<a class="btn btn-default" href="{{ route('viewCourse', $course->id) }}"><i class="fa fa-pencil" aria-hidden="true"></i> Redaguoti</a>
			    		@if (!count($course->group))
			    		<a class="btn btn-default" href="{{ route('deleteCourse', $course->id) }}"><i class="fa fa-times"  aria-hidden="true"></i> Ištrinti</a>
			    		@endif
			    		</td>
			    	</tr>
			    	@endforeach
			    </tbody>
			</table>

		</div>
	</div>
</div>
<div class="col-md-8 col-md-offset-2">
	<div class="panel panel-default">
		<div class="panel-heading">
	    	<h3 class="panel-title">Pridėti kursą</h3>
		</div>
		<div class="panel-body">
		
			<form class="form-horizontal" role="form" action="{{ route('addCourse') }}" method="post">
				{{ csrf_field() }}
				
				<div class="form-group{{ $errors->has('course') ? ' has-error' : '' }}">
					<label class="col-sm-4 control-label">Kursas</label>
					<div class="col-sm-6">
						<input class="form-control" name="course" type="text" value="{{ old('course') }}">
						@if ($errors->has('course'))
                        	<span class="help-block">
                            	<strong>{{ $errors->first('course') }}</strong>
							</span>
                        @endif
					</div>
				</div>
				
				<div class="input-group">
					<div class="col-sm-6 col-md-offset-4">
						<input type="submit" class="btn btn-primary" value="Pridėti">
					</div>
				</div>
				
			</form>
		
		</div>
	</div>
</div>

@endsection