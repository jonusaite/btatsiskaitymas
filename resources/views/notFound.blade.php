@extends('layouts.app')

@section('content')

    <div class="col-sm-9">
        <div class="panel panel-default">
            <div class="panel-heading">Puslapis nerastas!</div>

            <div class="panel-body">

                    Puslapis, kurio ieškojote, neegzistuoja...

            </div>
        </div>
    </div>


@endsection
