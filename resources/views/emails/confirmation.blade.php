@component('mail::message')
# Sveiki,

Jūsų paskyra buvo patvirtinta. Jums priskirta rolė – {{ $role }}.

@component('mail::button', ['url' => $url, 'color' => 'blue'])
Prisijungti
@endcomponent

Pagarbiai <br>
Baltic Talents komanda
@endcomponent
