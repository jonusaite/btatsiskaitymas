@component('mail::message')
# Sveiki

Jūs gavote žinutę nuo {{ Auth::user()->name }} {{ Auth::user()->surname }} Baltic Talents sistemoje:

@component('mail::panel')
{!! $message->message !!}
@endcomponent

@component('mail::button', ['url' => $url, 'color' => 'blue'])
Peržiūrėti žinutes
@endcomponent

Pagarbiai <br>
Baltic Talents komanda
@endcomponent
