@extends('layouts.app')
@section('content')

<div class="col-md-12">
  <h1>Išsiųsti laiškai</h1>
</div>

@foreach ($messages as $message)
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading">
	    	<h3 class="panel-title">Gavėjai: 
	    	@foreach ($message->user as $recipient)
	    	{{ $recipient->name }} {{ $recipient->surname }}
	    	@endforeach
	    	</h3>
		</div>
		<div class="panel-body table-responsive">
			<strong>{{ $message->created_at }} {{ $message->subject }}</strong>
			{!! $message->message !!}
		</div>
	</div>
</div>
@endforeach

@endsection