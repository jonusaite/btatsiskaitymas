@extends('layouts.app')
@section('content')
<div class="col-md-8 col-md-offset-2">
	<div class="panel panel-default">
		<div class="panel-heading">
	    	<h3 class="panel-title">Siųsti el. laišką</h3>
		</div>
		<div class="panel-body">

			<form  class="form-horizontal" role="form" action="{{ route('sendMassEmail') }}" method="post">
				{{ csrf_field() }}
				
				<div class="form-group{{ $errors->has('recipient') ? ' has-error' : '' }}">
					<label class="col-sm-4 control-label">Grupė</label>
					<div class="col-sm-6">
						<select name="recipient" class="form-control">
							<option value="">- Pasirinkite gavėją</option>
							@foreach ($groups as $group)
								<option value="{{ $group->id }}"{{ old('recipient') == $group->id ? ' selected' : '' }}>{{ $group->id }} {{ $group->course->course }}</option>
							@endforeach
						</select>
						@if ($errors->has('recipient'))
                        	<span class="help-block">
                            	<strong>{{ $errors->first('recipient') }}</strong>
							</span>
                        @endif
					</div>
				</div>
				
				<div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
					<label class="col-sm-4 control-label">Tema</label>
					<div class="col-sm-6">
						<input class="form-control" name="subject" type="text" value="{{ old('subject') }}">
						@if ($errors->has('subject'))
                        	<span class="help-block">
                            	<strong>{{ $errors->first('subject') }}</strong>
							</span>
                        @endif
					</div>
				</div>
				
				<div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
					<div class="col-sm-12">
						<textarea id="editor" class="form-control" rows="5" name="message">{{ htmlentities(old('message')) }}</textarea>
						@if ($errors->has('message'))
                        	<span class="help-block">
                            	<strong>{{ $errors->first('message') }}</strong>
							</span>
                        @endif
					</div>
				</div>
				
				<div class="input-group">
					<div class="col-sm-6 col-md-offset-4">
						<input type="submit" class="btn btn-primary" value="Išsiųsti">
					</div>
				</div>
				
			</form>
		
		</div>
	</div>
</div>

@endsection