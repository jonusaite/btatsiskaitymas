@extends('layouts.app')
@section('content')

<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading">
	    	<h3 class="panel-title">@if (isset($contact)) {{ $contact->subject }} @endif
	    	
	    	@if (Auth::user()->role == 1)
	    	<a class="btn btn-default" href="{{ route('editContact') }}">Redaguoti</a>
	    	@endif
	    	
	    	</h3>
		</div>
		<div class="panel-body table-responsive">
			@if (isset($contact)) {!! $contact->content !!} @endif
		</div>
	</div>
</div>

@endsection