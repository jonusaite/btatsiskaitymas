@extends('layouts.app')
@section('content')

<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading">
	    	<h3 class="panel-title">Grupės <strong> {{ $group->id }} {{ $group->course->course }} {{ $group->starts }} – {{ $group->ends }}</strong> studentai
	    	<a class="btn btn-default col-sm-offset-6" href="{{ route('addGroupStudentsView', $group->id) }}">Pridėti studentų</a>
	    	</h3>
		</div>
		<div class="panel-body table-responsive">
			<table class="table table-hover">
		    	<thead>
		    		<tr>
			    		<th>Vardas</th>
			    		<th>Pavardė</th>
			    		<th>El. paštas</th>
			    		<th>Telefonas</th>
			    		<th>Adresas</th>
			    		<th>Miestas</th>
			    		<th>Praktikos vieta</th>
			    		<th>Veiksmai</th>
			    	</tr>
			    </thead>
			    <tbody>
			    	@foreach ($group->user as $student)
			    	<tr>
			    		<td>{{ $student->name }}</td>
			    		<td>{{ $student->surname }}</td>
			    		<td>{{ $student->email }}</td>
			    		<td>{{ $student->phone }}</td>
			    		<td>{{ $student->address }}</td>
			    		<td>{{ $student->city }}</td>
			    		<td>
			    		@if ($student->group_user->where('group_id', $group->id)->first()->company_id)
			    		{{ $student->group_user->where('group_id', $group->id)->first()->company->company }}
			    		@endif
			    		</td>
			    		<td>
			    		<a class="btn btn-default" href="{{ route('viewStudentCompany', [$group->id, $student->id]) }}"><i class="fa fa-pencil" aria-hidden="true"></i> Praktikos vieta</a>
			    		<a class="btn btn-default" href="{{ route('deleteGroupStudent', [$group->id, $student->id]) }}"><i class="fa fa-times" aria-hidden="true"></i> Ištrinti</a>
			    		</td>
			    	</tr>
			    	@endforeach
			    </tbody>
			</table>

		</div>
	</div>
</div>

@endsection