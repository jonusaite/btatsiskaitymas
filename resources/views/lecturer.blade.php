@extends('layouts.app')
@section('content')

<div class="col-md-4">
	<div class="panel panel-default">
		<div class="panel-heading">
	    	<h3 class="panel-title">Informacija apie dėstytoją</h3>
		</div>
		<div class="panel-body table-responsive">
			{{ $lecturer->name }} {{ $lecturer->surname }} <br>
			Telefonas: {{ $lecturer->phone }} <br>
			Miestas: {{ $lecturer->city }}
			</div>
	</div>
</div>

<div class="col-md-8">
	<div class="panel panel-default">
		<div class="panel-heading">
	    	<h3 class="panel-title">Siųsti el. laišką dėstytojui</h3>
		</div>
		<div class="panel-body">

			<form  class="form-horizontal" role="form" action="{{ route('sendEmail') }}" method="post">
				{{ csrf_field() }}
				
				<input type="hidden" name="recipient" value="{{ $lecturer->id }}">						
				
				<div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
					<label class="col-sm-4 control-label">Tema</label>
					<div class="col-sm-6">
						<input class="form-control" name="subject" type="text" value="{{ old('subject') }}">
						@if ($errors->has('subject'))
                        	<span class="help-block">
                            	<strong>{{ $errors->first('subject') }}</strong>
							</span>
                        @endif
					</div>
				</div>
				
				<div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
					<div class="col-sm-12">
						<textarea id="editor" class="form-control" rows="5" name="message">{{ htmlentities(old('message')) }}</textarea>
						@if ($errors->has('message'))
                        	<span class="help-block">
                            	<strong>{{ $errors->first('message') }}</strong>
							</span>
                        @endif
					</div>
				</div>
				
				<div class="input-group">
					<div class="col-sm-6 col-md-offset-4">
						<input type="submit" class="btn btn-primary" value="Išsiųsti">
					</div>
				</div>
				
			</form>
		
		</div>
	</div>
</div>

@endsection