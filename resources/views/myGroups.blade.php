@extends('layouts.app')
@section('content')
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading">
	    	<h3 class="panel-title">Grupių sąrašas</h3>
		</div>
		<div class="panel-body table-responsive">
			<table class="table table-hover">
		    	<thead>
		    		<tr>
			    		<th>Grupė</th>
			    		<th>Kursas</th>
			    		<th>Dėstytojai</th>
			    		<th>Pradžios data</th>
			    		<th>Pabaigos data</th>
			    		<th>Praktikos vieta</th>
			    		<th>Veiksmai</th>
			    	</tr>
			    </thead>
			    <tbody>
			    	@foreach ($groups as $group)
			    	<tr>
			    		<td>{{ $group->id }}</td>
			    		<td>{{ $group->course->course }}</td>
			    		<td>
			    		@foreach ($group->lecturer as $lecturer)
			    			<a class="btn btn-default" href="{{ route('lecturer', $lecturer->id) }}">{{ $lecturer->name }} {{ $lecturer->surname }}</a>
			    		@endforeach
			    		</td>
			    		<td>{{ $group->starts }}</td>
			    		<td>{{ $group->ends }}</td>
			    		<td>
			    		@if ($group->group_user->where('user_id', Auth::user()->id)->first()->company_id)
			    			{{ $group->group_user->where('user_id', Auth::user()->id)->first()->company->company }}
			    		@endif
			    		</td>
			    		<td>
			    		<a class="btn btn-default" href="{{ route('myCompany', $group->id) }}"><i class="fa fa-pencil" aria-hidden="true"></i> Praktikos vieta</a>
			    		</td>
			    	</tr>
			    	@endforeach
			    </tbody>
			</table>

		</div>
	</div>
</div>

@endsection