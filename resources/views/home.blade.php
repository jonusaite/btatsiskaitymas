@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
            
            @if (Auth::user()->role == NULL)
                <div class="panel-heading">Laukiama patvirtinimo</div>
                <div class="panel-body">
                    Jūs esate prisijungęs (-usi). Kad galėtumėte naudotis svetainės funkcijomis, Jus turi  patvirtinti dėstytojas.
                </div>
                
            @else
            	<div class="panel-heading">Sveiki prisijungę, {{ Auth::user()->name }} {{ Auth::user()->surname }}</div>
                <div class="panel-body">
                    Jūs esate prisijungęs (-usi) kaip 
                    @if (Auth::user()->role == 1) dėstytojas.
                    @else studentas.
                    @endif
                </div>
            @endif
                
            </div>
        </div>
    </div>
</div>
@endsection
