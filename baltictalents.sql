-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 2017 m. Lie 13 d. 01:54
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `baltictalents`
--

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `companies`
--

CREATE TABLE `companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `companies`
--

INSERT INTO `companies` (`id`, `company`, `phone`, `address`, `city`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'Įmonė 2', NULL, NULL, 'Klaipėda', NULL, '2017-06-03 14:42:36', '2017-06-03 14:42:36'),
(3, 'Pavadinimas 3', '+37062222222', 'Gatvės g. 8', 'Kaunas', NULL, '2017-06-03 14:43:08', '2017-06-03 14:43:08'),
(11, 'UAB 123', NULL, NULL, NULL, NULL, '2017-06-05 06:28:52', '2017-06-05 06:28:52'),
(12, 'Uab 5', '+37061111111', NULL, 'Vilnius', NULL, '2017-07-12 18:53:02', '2017-07-12 18:53:02');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `company_course`
--

CREATE TABLE `company_course` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `positions` int(11) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `company_course`
--

INSERT INTO `company_course` (`id`, `company_id`, `course_id`, `positions`, `remember_token`, `created_at`, `updated_at`) VALUES
(16, 2, 1, 4, NULL, '2017-06-03 16:20:47', '2017-07-12 20:36:42'),
(18, 3, 2, 1, NULL, '2017-06-03 16:43:12', '2017-07-12 20:36:15'),
(25, 2, 2, 2, NULL, '2017-06-04 12:30:52', '2017-07-12 20:36:16'),
(28, 3, 1, 3, NULL, '2017-06-04 12:39:42', '2017-07-12 20:36:57'),
(29, 2, 9, 0, NULL, '2017-06-05 06:29:24', '2017-06-05 06:38:56'),
(30, 11, 1, 2, NULL, '2017-06-05 06:41:25', '2017-06-05 06:41:25'),
(31, 12, 2, 1, NULL, '2017-07-12 18:53:15', '2017-07-12 18:53:15');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `contacts`
--

INSERT INTO `contacts` (`id`, `subject`, `content`, `remember_token`, `created_at`, `updated_at`) VALUES
(6, 'Kontaktai', '<p><img style=\"float: left;\" src=\"https://collegian.com/wp-content/uploads/2017/01/ordinateur-de-bureau-pc-1456070535WEH.jpg\" alt=\"\" width=\"270\" height=\"180\" /></p>\r\n<p><strong>&#160;T</strong><strong>elefonas</strong>: +37060000000</p>\r\n<p><strong>&#160;Adresas</strong>: Gatv&#279; g. 123</p>\r\n<p><strong>&#160;Miestas</strong>: Klaip&#279;da</p>', NULL, '2017-06-04 16:18:19', '2017-06-05 06:30:57');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `courses`
--

CREATE TABLE `courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `course` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `courses`
--

INSERT INTO `courses` (`id`, `course`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Backend', NULL, '2017-05-31 11:29:04', '2017-05-31 11:29:04'),
(2, 'Frontend', NULL, '2017-05-31 11:33:17', '2017-05-31 11:33:17'),
(9, 'Java', NULL, '2017-05-31 12:15:40', '2017-05-31 12:17:15');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `groups`
--

CREATE TABLE `groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `course_id` int(11) NOT NULL,
  `starts` date NOT NULL,
  `ends` date NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `groups`
--

INSERT INTO `groups` (`id`, `course_id`, `starts`, `ends`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 1, '2017-03-27', '2017-07-27', NULL, '2017-05-31 14:44:38', '2017-05-31 14:44:38'),
(3, 2, '2017-04-01', '2017-08-02', NULL, '2017-05-31 14:48:11', '2017-05-31 14:58:27'),
(4, 1, '2017-05-15', '2017-09-15', NULL, '2017-06-01 15:11:23', '2017-06-01 15:11:23'),
(6, 9, '2017-05-01', '2017-09-01', NULL, '2017-06-04 12:25:57', '2017-06-04 12:25:57');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `group_lecturer`
--

CREATE TABLE `group_lecturer` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `group_lecturer`
--

INSERT INTO `group_lecturer` (`id`, `group_id`, `user_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 3, 5, NULL, '2017-06-04 07:36:56', '2017-06-04 07:36:56'),
(4, 4, 1, NULL, '2017-06-04 07:37:04', '2017-06-04 07:37:04'),
(5, 2, 1, NULL, '2017-06-04 07:38:18', '2017-06-04 07:38:18'),
(7, 2, 5, NULL, '2017-06-05 06:27:30', '2017-06-05 06:27:30');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `group_user`
--

CREATE TABLE `group_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `group_user`
--

INSERT INTO `group_user` (`id`, `group_id`, `user_id`, `company_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 3, 2, 2, NULL, NULL, '2017-07-12 20:36:16'),
(5, 3, 3, 3, NULL, '2017-06-03 18:33:49', '2017-06-03 19:58:23'),
(9, 4, 2, 2, NULL, '2017-06-04 11:51:25', '2017-07-12 20:36:42'),
(10, 6, 2, NULL, NULL, '2017-06-04 12:26:07', '2017-06-04 12:26:07'),
(12, 2, 3, NULL, NULL, '2017-06-05 06:28:16', '2017-07-12 20:36:57');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `sender_id` int(11) NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `messages`
--

INSERT INTO `messages` (`id`, `sender_id`, `subject`, `message`, `remember_token`, `created_at`, `updated_at`) VALUES
(4, 3, 'Gauta nauja žinutė', '<p>Bandau</p>', NULL, '2017-06-04 10:37:17', '2017-06-04 10:37:17'),
(5, 1, 'Labas', '<p>&#381;inut&#279;</p>', NULL, '2017-06-04 10:38:00', '2017-06-04 10:38:00'),
(6, 1, 'Labas', '<p>Sveiki</p>', NULL, '2017-06-04 10:55:04', '2017-06-04 10:55:04'),
(7, 3, 'Dar kartą', '<p>Bandymas. Labas <strong>rytas</strong>! <img src=&#34;https://camo.mybb.com/e01de90be6012adc1b1701dba899491a9348ae79/687474703a2f2f7777772e6a71756572797363726970742e6e65742f696d616765732f53696d706c6573742d526573706f6e736976652d6a51756572792d496d6167652d4c69676874626f782d506c7567696e2d73696d706c652d6c69676874626f782e6a7067&#34; alt=&#34;&#34; width=&#34;259&#34; height=&#34;189&#34; /></p>', NULL, '2017-06-04 11:04:00', '2017-06-04 11:04:00'),
(8, 3, 'Gauta nauja žinutė', '<p><img src=&#34;https://www.smashingmagazine.com/wp-content/uploads/2015/06/10-dithering-opt.jpg&#34; alt=&#34;&#34; width=&#34;242&#34; height=&#34;162&#34; /></p>\r\n<p>Labas.</p>\r\n<p>Pel&#279;da <span style=&#34;color: #ff9900;&#34;>tau</span>.</p>', NULL, '2017-06-04 11:07:42', '2017-06-04 11:07:42'),
(9, 3, 'Gauta nauja žinutė', '<p><img src=\"https://www.smashingmagazine.com/wp-content/uploads/2015/06/10-dithering-opt.jpg\" alt=\"\" width=\"1199\" height=\"803\" /></p>', NULL, '2017-06-04 11:08:30', '2017-06-04 11:08:30'),
(10, 3, 'Gauta nauja žinutė', '<p>Ouuuu sdf <strong>fhs</strong> hfds</p>', NULL, '2017-06-04 11:21:24', '2017-06-04 11:21:24'),
(11, 3, 'fsd s', '<p>asf sfsrfs</p>', NULL, '2017-06-04 11:33:28', '2017-06-04 11:33:28'),
(12, 3, 'Gauta nauja žinutė', '<p>ad fs sdfffffffffffffffff</p>', NULL, '2017-06-04 11:34:31', '2017-06-04 11:34:31'),
(13, 3, 'Gauta nauja žinutė', '<p>ffff ffff</p>', NULL, '2017-06-04 11:34:43', '2017-06-04 11:34:43'),
(14, 1, 'Gauta nauja žinutė', '<p>jkkhpuh</p>', NULL, '2017-06-04 11:43:30', '2017-06-04 11:43:30'),
(15, 3, 'Gauta nauja žinutė', '<p>lalalala</p>', NULL, '2017-06-04 11:46:45', '2017-06-04 11:46:45'),
(16, 3, 'Labas', '<p>Bandau</p>', NULL, '2017-06-04 15:01:53', '2017-06-04 15:01:53'),
(17, 3, 'Dar', '<p>kart&#261;</p>', NULL, '2017-06-04 15:04:06', '2017-06-04 15:04:06'),
(18, 3, 'Gauta nauja žinutė', '<p>Bandyamas nr. 2</p>', NULL, '2017-06-04 15:10:34', '2017-06-04 15:10:34'),
(19, 3, 'Patikrinimas', '<p>123</p>', NULL, '2017-06-04 15:12:55', '2017-06-04 15:12:55'),
(20, 3, 'Ilgas tekstas', '<h1>BalticTalents student&#371; duomen&#371; baz&#279;</h1>\r\n<p><strong>Sistem&#261; sudaryt&#371; dviej&#371; tip&#371; vartotojai:</strong></p>\r\n<ol>\r\n<li>d&#279;stytojas / administratorius</li>\r\n<li>studentai</li>\r\n</ol>\r\n<p>Kiekvienas vartotojas registruojasi savaranki&#353;kai, bet prisijung&#281;s nieko nemato ir negali daryti, kol d&#279;stytojas/administratorius nepriskiria rol&#279;s ir nepatvirtina. Po patvirtinimo studentui ar d&#279;stytojui turi b&#363;ti i&#353;si&#371;stas el. lai&#353;kas su prane&#353;imu apie patvirtinim&#261;.</p>\r\n<p>Registracijos metu privaloma pateikti &#353;i informacija: Vardas, Pavard&#279;, El. pa&#353;to adresas, Telefonas, Adresas, Miestas</p>\r\n<p>D&#279;stytojas gali sukurti naujus kursus (backend, frontend, Java ....), koreguoti ir i&#353;trinti</p>\r\n<p>D&#279;stytojas gali sukurti naujas grupes (jas sudaro kursas (pvz. backend), prad&#382;ios data, numatomos pabaigos data) , koreguoti ir i&#353;trinti</p>\r\n<p>D&#279;stytojas gali priskirti prie grup&#279;s studentus, koreguoti ir pa&#353;alinti.</p>\r\n<p>D&#279;stytojas turi gal&#279;ti i&#353;si&#353;usti visiems studentams masin&#303; el. lai&#353;k&#261; (visai grupei, bet kuriai) arba vienam studentui, &#353;ie lai&#353;kai turi i&#353;sisaugoti sistemoje ir prisijungus juos rodyti pagal dat&#261; (sen&#279;jimo tvarka, ka&#382;kas pana&#353;aus &#303; forum&#261;).</p>\r\n<p>Studentai taip pat turi gal&#279;ti i&#353;si&#371;sti lai&#353;k&#261; visiems studentams, arba d&#279;stytojui (ta&#269;iau tik savo grupei ir savo d&#279;stytojui), jei siun&#269;iamas el. lai&#353;kas d&#279;stytojui, jis rodomas tik d&#279;stytojo ir to studento paskyroje.</p>\r\n<p>Sistemoje turi b&#363;ti kontaktin&#279;s informacijos langas kur pateikiamas adresas, administracijos kontaktai ir kt. informacija (&#353;i informacija turi b&#363;ti koreguojama tinyMCE (&#303; assets, suintegruoti su mixinu) redaktoriumi).</p>\r\n<p>D&#279;stytojas visiems studentams, o studentai savo paskyrai gali priskirti praktikos viet&#261; (praktikos viet&#261;, praktikos vietos &#8211; atskira lentel&#279; kur sura&#353;ytos &#303;mon&#279;s). &#302;mones gali koreguoti d&#279;stytojas.</p>\r\n<p>&#160;</p>\r\n<p>&#160;</p>\r\n<h1>Baltictalents mokomosios med&#382;iagos DB</h1>\r\n<p><strong>Sistem&#261; sudaryt&#371; dviej&#371; tip&#371; vartotojai:</strong></p>\r\n<ol>\r\n<li>d&#279;stytojas / administratorius</li>\r\n<li>studentai</li>\r\n</ol>\r\n<p>Kiekvienas vartotojas registruojasi savaranki&#353;kai, bet prisijung&#281;s nieko nemato ir negali daryti, kol d&#279;stytojas/administratorius nepriskiria rol&#279;s ir nepatvirtina. Po patvirtinimo studentui ar d&#279;stytojui turi b&#363;ti i&#353;si&#371;stas el. lai&#353;kas su prane&#353;imu apie patvirtinim&#261;.</p>\r\n<p>Registracijos metu privaloma pateikti &#353;i informacija: Vardas, Pavard&#279;, El. pa&#353;to adresas, Telefonas</p>\r\n<p>D&#279;stytojas gali sukurti naujus kursus (backend, frontend, Java ....), koreguoti ir i&#353;trinti</p>\r\n<p>D&#279;stytojas gali sukurti naujas grupes (jas sudaro kursas (pvz. backend), prad&#382;ios data, numatomos pabaigos data) , koreguoti ir i&#353;trinti</p>\r\n<p>D&#279;stytojas gali priskirti prie grup&#279;s studentus, koreguoti ir pa&#353;alinti.</p>\r\n<p>D&#279;stytojas gali &#303;kelti mokom&#261;j&#261; med&#382;iag&#261; (failus) ir priskirti prie tam tikrai grupei. Failo duomenis DB turi sudaryti: pavadinimas, apra&#353;as, data (kada vyks &#353;i paskaita) ir failai (t.y. vienai paskaitai gali b&#363;ti &#303;kelti daugiau nei vienas failas).</p>\r\n<p>Tai yra vienai grupei tur&#279;t&#371; b&#363;ti pateikiamas&#160; toks s&#261;ra&#353;as:</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td width=\"160\">\r\n<p>Data</p>\r\n</td>\r\n<td width=\"160\">\r\n<p>Pavadinimas</p>\r\n</td>\r\n<td width=\"160\">\r\n<p>Apra&#353;as</p>\r\n</td>\r\n<td width=\"160\">\r\n<p>Failai</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width=\"160\">\r\n<p>2017.04.06</p>\r\n</td>\r\n<td width=\"160\">\r\n<p>PHP masyvai</p>\r\n</td>\r\n<td width=\"160\">\r\n<p>Vienma&#269;iai masyvai ir j&#371; panaudojimas</p>\r\n</td>\r\n<td width=\"160\">\r\n<p>Skaidr&#279;s (parsisi&#371;sti)</p>\r\n<p>Programinia kodai (parsisi&#371;sti)</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width=\"160\">\r\n<p>2017.04.06</p>\r\n</td>\r\n<td width=\"160\">\r\n<p>PHP daugiama&#269;iai masyvai</p>\r\n</td>\r\n<td width=\"160\">\r\n<p>Vienma&#269;iai masyvai ir j&#371; panaudojimas</p>\r\n</td>\r\n<td width=\"160\">\r\n<p>Skaidr&#279;s (parsisi&#371;sti)</p>\r\n<p>Programinia kodai (parsisi&#371;sti)</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&#160;</p>\r\n<p>D&#279;stytojas gali padaryti jog kai kurie failai bus nematomi, o kai kurie bus matomi studentams, taip pat d&#279;stytojas turi tur&#279;ti galimybe redaguoti apra&#353;us, pa&#353;alinti failus ar visus s&#261;ra&#353;us.</p>\r\n<p>D&#279;stytojas turi gal&#279;ti i&#353;si&#353;usti visiems studentams masin&#303; el. lai&#353;k&#261; (visai grupei) .</p>\r\n<p>Sistemoje turi b&#363;ti kontaktin&#279;s informacijos langas kur pateikiamas adresas, administracijos kontaktai ir kt. informacija (&#353;i informacija turi b&#363;ti koreguojama tinyMCE redaktoriumi).</p>\r\n<p>&#160;</p>\r\n<p>&#160;</p>', NULL, '2017-06-04 15:13:37', '2017-06-04 15:13:37'),
(21, 5, 'y', '<p>hey</p>', NULL, '2017-06-04 18:39:37', '2017-06-04 18:39:37'),
(22, 1, 'Gauta nauja žinutė', '<p>Naujas laiskas</p>', NULL, '2017-06-05 06:31:31', '2017-06-05 06:31:31');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `message_user`
--

CREATE TABLE `message_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `message_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `message_user`
--

INSERT INTO `message_user` (`id`, `message_id`, `user_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 4, 2, NULL, '2017-06-04 10:37:17', '2017-06-04 10:37:17'),
(2, 5, 3, NULL, '2017-06-04 10:38:00', '2017-06-04 10:38:00'),
(3, 6, 2, NULL, '2017-06-04 10:55:05', '2017-06-04 10:55:05'),
(4, 6, 3, NULL, '2017-06-04 10:55:05', '2017-06-04 10:55:05'),
(5, 7, 3, NULL, '2017-06-04 11:04:00', '2017-06-04 11:04:00'),
(6, 8, 3, NULL, '2017-06-04 11:07:42', '2017-06-04 11:07:42'),
(7, 9, 3, NULL, '2017-06-04 11:08:30', '2017-06-04 11:08:30'),
(8, 10, 1, NULL, '2017-06-04 11:21:24', '2017-06-04 11:21:24'),
(9, 11, 1, NULL, '2017-06-04 11:33:28', '2017-06-04 11:33:28'),
(10, 12, 1, NULL, '2017-06-04 11:34:31', '2017-06-04 11:34:31'),
(11, 13, 2, NULL, '2017-06-04 11:34:43', '2017-06-04 11:34:43'),
(12, 13, 3, NULL, '2017-06-04 11:34:43', '2017-06-04 11:34:43'),
(13, 15, 2, NULL, '2017-06-04 11:46:45', '2017-06-04 11:46:45'),
(14, 15, 3, NULL, '2017-06-04 11:46:45', '2017-06-04 11:46:45'),
(15, 16, 1, NULL, '2017-06-04 15:01:53', '2017-06-04 15:01:53'),
(16, 17, 1, NULL, '2017-06-04 15:04:06', '2017-06-04 15:04:06'),
(17, 18, 1, NULL, '2017-06-04 15:10:34', '2017-06-04 15:10:34'),
(18, 19, 1, NULL, '2017-06-04 15:12:55', '2017-06-04 15:12:55'),
(19, 20, 1, NULL, '2017-06-04 15:13:37', '2017-06-04 15:13:37'),
(20, 21, 1, NULL, '2017-06-04 18:39:37', '2017-06-04 18:39:37'),
(21, 22, 2, NULL, '2017-06-05 06:31:31', '2017-06-05 06:31:31');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(6, '2014_10_12_000000_create_users_table', 1),
(7, '2014_10_12_100000_create_password_resets_table', 1),
(8, '2017_05_31_132540_Course', 2),
(9, '2017_05_31_132743_Group', 2),
(14, '2017_05_31_182410_group_user', 3),
(16, '2017_06_01_203218_create_companies_table', 4),
(17, '2017_06_02_154734_create_positions_table', 4),
(19, '2017_06_02_170904_group_user2', 5),
(20, '2017_06_04_100138_group_lecturer', 6),
(21, '2017_06_04_130610_messages', 7),
(22, '2017_06_04_130652_message_user', 7),
(24, '2017_06_04_183614_contact', 8);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` smallint(6) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `email`, `phone`, `address`, `city`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Aistė', 'Jonušaitė', 'jonusaitea@gmail.com', '+37060908570', 'Karlskronos g. 3 - 27', 'Klaipėda', '$2y$10$SMAhltW5wHC8q8WtVMgbyu8zTysSC/t27vGnAarUCZl7rLUylH2qO', 1, 'kFyp7MLEmgOKkPNwGCPt8vKJvKyrtl4oefdrKliYzA81qJuWjVmTAlMx7oZq', '2017-05-28 11:01:07', '2017-05-28 11:59:03'),
(2, 'Vardenis', 'Pavardenis', 'undinele1990@gmail.com', '+37060000000', 'Gatvė 5', 'Vilnius', '$2y$10$HtoZ4Vcl8AiSVK0zWFoNkeVkegqZIiIIwPjBYX53s7w.4GXyB2Xza', 2, 'FZInUulXfuQ6bkLir3L64YA5Aci2k0k7zRnL0NJvToNX98p1wOy6VDDUnPpZ', '2017-05-29 03:19:28', '2017-05-30 12:09:10'),
(3, 'Vardė', 'Pavardė', 'taropaslaugos@gmail.com', '+37069999999', 'Aikštė 10', 'Kaunas', '$2y$10$2ab8Gpl8OruuFErrGFWcFOqCXaxnzRbigBZ.CvDx/sNUgrwDbf0yC', 2, 'OnlyW2zyoISRixkVNaAJwX4Fg40VMVv9JkKrxFwqziAizIJT1thS7IzM5R5q', '2017-05-31 09:41:39', '2017-06-04 18:33:13'),
(4, 'Petras', 'Petraitis', 'aiste.psichologe@gmail.com', '+37061111111', 'Alėja 3', 'Klaipėda', '$2y$10$.M.AWr0A..utZBThQuJ.uOuScEc3r1Tq/yqVxjVrKBiO0oyYjaIxe', NULL, '9sNZsXSInNzcsXjVOi1Nd62QW5RBCBoO26xOiXGo2lRq5ZXCAWpUEZR7m2JQ', '2017-06-01 16:42:44', '2017-06-04 18:31:36'),
(5, 'Dėstytojas', 'Pavardauskas', 'rankdarbiaisirdies@gmail.com', '+37060101010', 'Adreso g. 90', 'Vilnius', '$2y$10$8IuBdrKqj0H1xWI/ovzfY.Dnd2uH18zUgvfAdWiS5ffEgOle8VeTe', 1, 'sQZdUIoqQQvQXsAT8JVgFWGRQDcSgNSuxPrWdqEgR4GUS7n6VNKBl9s34MNC', '2017-06-04 06:59:03', '2017-06-05 06:25:44'),
(6, 'dfsd', 'fsfs', 'aiste@bla.lt', 'dffsf', 'sfsdf', 'sfsdf', '$2y$10$c6b1cauJt/OrsW.4MS.xoeQCr9UqvlxMGsm1zE9kmvz3IpQIJenCu', NULL, 'LAnxv268A2fAB9q0WSfJESWxz5CZziJeWPjpQmIHP4GxQAQGdE0SfJpW7D2i', '2017-06-05 06:38:09', '2017-06-05 06:38:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_course`
--
ALTER TABLE `company_course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_lecturer`
--
ALTER TABLE `group_lecturer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_user`
--
ALTER TABLE `group_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message_user`
--
ALTER TABLE `message_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `company_course`
--
ALTER TABLE `company_course`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `group_lecturer`
--
ALTER TABLE `group_lecturer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `group_user`
--
ALTER TABLE `group_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `message_user`
--
ALTER TABLE `message_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
