<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('notFound', function() {
	return view('notFound');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/contact', 'ContactController@contactView')->name('contact');

Route::group(['middleware' => 'auth'], function () {
	Route::group(['middleware' => 'role:1'], function () {
		Route::get('/roles', 'AdministratorController@addRoleView')->name('roles');
		Route::post('/addRole', 'AdministratorController@addRole')->name('addRole');
		Route::get('/deleteRole/{id}', 'AdministratorController@deleteRole')->name('deleteRole');
		Route::get('/courses', 'AdministratorController@viewCourses')->name('courses');
		Route::post('/addCourse', 'AdministratorController@addCourse')->name('addCourse');
		Route::get('/deleteCourse/{id}', 'AdministratorController@deleteCourse')->name('deleteCourse');
		Route::get('/viewCourse/{id}', 'AdministratorController@viewCourse')->name('viewCourse');
		Route::post('/editCourse/{id}', 'AdministratorController@editCourse')->name('editCourse');
		Route::get('/groups', 'AdministratorController@viewGroups')->name('groups');
		Route::post('/addGroup', 'AdministratorController@addGroup')->name('addGroup');
		Route::get('/deleteGroup/{id}', 'AdministratorController@deleteGroup')->name('deleteGroup');
		Route::get('/viewGroup/{id}', 'AdministratorController@viewGroup')->name('viewGroup');
		Route::get('/viewCourseGroups/{id}', 'AdministratorController@viewCourseGroups')->name('viewCourseGroups');
		Route::post('/editGroup/{id}', 'AdministratorController@editGroup')->name('editGroup');
		Route::post('/addLecturer/{id}', 'AdministratorController@addLecturer')->name('addLecturer');
		Route::get('/deleteLecturer/{id}/{lecturerId}', 'AdministratorController@deleteLecturer')->name('deleteLecturer');
		Route::get('/groupStudents/{id}', 'AdministratorController@groupStudents')->name('groupStudents');
		Route::get('/addGroupStudentsView/{id}', 'AdministratorController@addGroupStudentsView')->name('addGroupStudentsView');
		Route::post('/addGroupStudents/{id}', 'AdministratorController@addGroupStudents')->name('addGroupStudents');
		Route::get('/deleteGroupStudent/{id}/{studentId}', 'AdministratorController@deleteGroupStudent')->name('deleteGroupStudent');
		Route::get('/companies', 'AdministratorController@viewCompanies')->name('companies');
		Route::post('/addCompany', 'AdministratorController@addCompany')->name('addCompany');
		Route::post('/editCompany/{id}', 'AdministratorController@editCompany')->name('editCompany');
		Route::get('/deleteCompany/{id}', 'AdministratorController@deleteCompany')->name('deleteCompany');
		Route::get('/company/{id}', 'AdministratorController@viewCompany')->name('company');
		Route::post('/addPositions/{id}', 'AdministratorController@addPositions')->name('addPositions');
		Route::get('/deletePositions/{id}/{course_id}', 'AdministratorController@deletePositions')->name('deletePositions');
		Route::get('/viewStudentCompany/{groupId}/{studentId}', 'AdministratorController@viewStudentCompany')->name('viewStudentCompany');
		Route::post('/addStudentCompany/{groupId}/{studentId}', 'AdministratorController@addStudentCompany')->name('addStudentCompany');
		Route::get('/deleteStudentCompany/{groupId}/{studentId}/{companyId}', 'AdministratorController@deleteStudentCompany')->name('deleteStudentCompany');
		Route::get('/email', 'EmailController@viewSendEmail')->name('email');
		Route::get('/editContact', 'ContactController@editContact')->name('editContact');
		Route::post('/saveContact', 'ContactController@saveContact')->name('saveContact');
	});
	Route::group(['middleware' => 'role:1,2'], function () {
		Route::post('/sendEmail', 'EmailController@email')->name('sendEmail');
		Route::get('/massEmail', 'EmailController@viewSendMassEmail')->name('massEmail');
		Route::post('/sendMassEmail', 'EmailController@sendMassEmail')->name('sendMassEmail');
		Route::get('/emailReceived', 'EmailController@emailReceived')->name('emailReceived');
		Route::get('/emailSent', 'EmailController@emailSent')->name('emailSent');
	});
	Route::group(['middleware' => 'role:2'], function () {
		Route::get('/myGroups', 'StudentController@viewGroups')->name('myGroups');
		Route::get('/myCompany/{groupId}', 'StudentController@viewStudentCompany')->name('myCompany');
		Route::post('/addMyCompany/{groupId}', 'StudentController@addStudentCompany')->name('addMyCompany');
		Route::get('/deleteMyCompany/{groupId}/{companyId}', 'StudentController@deleteStudentCompany')->name('deleteMyCompany');
		Route::get('/lecturer/{id}', 'StudentController@lecturer')->name('lecturer');
	});
});